var express = require('express');
var path = require('path');
var fs = require('fs');
var bodyParser = require('body-parser');
var port = 5000;
var app = express();

var cors = require('cors');
app.use(cors());
var server = require('http');
server.createServer(app);

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000
}));


/*var enableCORS = function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
 /!* res.header('Access-Control-Allow-Origin', 'https://beta.wonderhfl.com');*!/

  /!*res.header('Access-Control-Allow-Origin', 'https://beta-admin.wonderhfl.com');*!/
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, token, Content-Length, X-Requested-With, *');
	if ('OPTIONS' === req.method || 'POST' === req.method ){
    var headers = {};
    headers["Access-Control-Allow-Origin"] = "*";
    headers["Access-Control-Allow-Methods"] = "GET,PUT,POST,DELETE,OPTIONS";
    headers["Access-Control-Allow-Credentials"] = true;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    headers["Access-Control-Allow-Headers"] = "Origin, Content-Type, Accept, Authorization, X-Request-With, X-CLIENT-ID, X-CLIENT-SECRET";
    res.writeHead(200, headers);
    res.end();
	} else {
	  next();
	}
};*/
var enableCORS = (req, res, next) => {
    // headers["Access-Control-Allow-Credentials"] = true;
    //headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    // res.header('Access-Control-Allow-Origin', 'https://beta-admin.wonderhfl.com');
    // res.header('Access-Control-Allow-Origin', 'https://beta.wonderhfl.com');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin,Content-Type, Content-Length, X-Requested-With,Accept, *');
    if ('OPTIONS' === req.method) {
        res.sendStatus(200);
    } else {
        next();
    }
};
app.all("/*", function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization, X-Request-With, X-CLIENT-ID, X-CLIENT-SECRET,*');
    next();
});
app.use(enableCORS);

// app.use(function(req, res, next) {
//   var oneof = false;
//   if(req.headers.origin) {
//       res.header('Access-Control-Allow-Origin', req.headers.origin);
//       oneof = true;
//   }
//   if(req.headers['access-control-request-method']) {
//       res.header('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
//       oneof = true;
//   }
//   if(req.headers['access-control-request-headers']) {
//       res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
//       oneof = true;
//   }
//   if(oneof) {
//       res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);
//   }

//   if (oneof && req.method == 'OPTIONS') {
//       res.send(200);
//   }
//   else {next();}
// });

// Routes
//require('./routes')(app);
const allRoutes = require('./routes')(app);

process.on('uncaughtException', function (err) {
    console.log('Caught exception: ' + err);
    console.log(err.stack);
});

app.use(express.static(__dirname + '/images'));


app.listen(5000, function () {
    console.log("Express server listening on port", 5000);
});

module.exports = app, allRoutes
